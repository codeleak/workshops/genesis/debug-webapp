# Debug Webapp

## Debugging locally a Tomcat application

- Download Tomcat server to be able to run the project: https://tomcat.apache.org/download-90.cgi
- Make sure `bin/catalina.sh` is executable (`chmod +x catalina.sh`)
- Import the project in IntelliJ
- Create run configuration for Tomcat
- Add `debug-webapp` artifact exploded
- Start a debugging session

## Debugging remotely a Wildfly application running in Docker

- Navigate to terminal and open project's directory
- Make sure `dist/webapp-demo.war` has valid priviliges (`755`)

`chmod 755 dist/webapp-demo.war`

- Build an image:

`docker build --tag=wildfly-webapp-demo .`

- Run the container:

`docker run -it -p 8080:8080 -p 9990:9990 -p 8787:8787 wildfly-webapp-demo`

- Make sure no errors in the console:
- Open the application in a browser: `http://localhost:8080/webapp-demo`
- Open IntelliJ and make sure project is imported
- Add new configuration: `Remote JVM Debug`
  - Set debug port to `8787`
- Start debugging session, set breakpoint and navigate to the application to trigger it

## Debuging remotely with Wildfly (no Docker)

http://blog.codeleak.pl/2017/06/remote-debugging-wildfly-application-in.html
